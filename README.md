# hadoop-lab

## eventproducer

com.griddynamics.eventproducer.Main generates 10000 random events 
and send them to given host:port which are passed via parameters. 
Category names from a to j. 
Product names are looking liike <category name><from 0 to 9>.
Date - in one week diapason.
Price - from 1 to 200. IP - random.

## Flume config (flumeconfig)

Flume is configured with flume-agent.properties. Launch string could be found in flume.sh

## Hive tables (hive-ddl)
- create-table.ddl is for events table creation
- create-partitions.sh is for partition creation
- create-geodata.sql is for geodata loading to hive

## Hive SQLs (hive-sql)
SQLs are located in src/main/resources, results in src/test/resources

NOTE: temporary table event_networks has been created because issues with subselect from lateral view. 

## Hive UDF (hive-udf)
com.griddynamics.GetNetworks returns a list of possible networks for each IP

## sqoop 
Not finished. Issues with access to hadoop

## MySQL tables (mysql-ddls)

## Spark jobs (spark)
Separate driver created for each task. There are two methods in each driver, first is operating with RDDs,
second is operating with DataFrames.
I have not tried to do the same with DataSets because of absense on Spark 2. TODO: create it and test locally. 

  

