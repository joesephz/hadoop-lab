package com.griddynamics;

import static org.junit.Assert.assertNotNull;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.hive.HiveContext;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

public class Top10ProductsDriverTest extends AbstractTest {
  private static DataFrame events;
  private static Top10ProductsDriver driver;

  @BeforeClass
  public static void beforeClass() {
    AbstractTest.beforeClass();
    sqlContext = new HiveContext(sc);
    driver = new Top10ProductsDriver(sc, sqlContext);
    events = loadEvents(sqlContext);
  }

  @Test
  public void process() {
    JavaRDD<Top10ProductsDriver.Top10Products> resultRDD = driver.process(events.toJavaRDD());
    assertNotNull(resultRDD);
    List<Top10ProductsDriver.Top10Products> result = resultRDD.collect();
    result.forEach(System.out::println);
  }

  @Test
  public void processWithDF() {
    DataFrame result = driver.process(events);
    assertNotNull(result);
    result.show();
  }
}
