/*SELECT count(name) as cnt, category
FROM events
GROUP BY category
ORDER BY cnt DESC
LIMIT 10;

SELECT country_name,
  sum(price) AS spendings
FROM (
  SELECT ev.price, geocountry.country_name
  FROM events ev, geo_ip geoip
  JOIN geo_country geocountry on geocountry.geoname_id = geoip.geoname_id
  WHERE checkipinsubnetwork(ev.ip, geoip.network)
) x
GROUP BY country_name
ORDER BY spendings DESC
LIMIT 10;


SELECT country_name,
  sum(price) AS spendings
FROM (
  SELECT DISTINCT ev.name, ev.price, geocountry.country_name
  FROM events ev, geo_ip geoip
  JOIN geo_country geocountry on geocountry.geoname_id = geoip.geoname_id
  WHERE checkipinsubnetwork(ev.ip, geoip.network)
) x
GROUP BY country_name
ORDER BY spendings DESC
LIMIT 10;


SELECT count (DISTINCT ev.name, ev.price, geocountry.country_name )
  FROM events ev, geo_ip geoip
  JOIN geo_country geocountry on geocountry.geoname_id = geoip.geoname_id
  WHERE checkipinsubnetwork(ev.ip, geoip.network)*/


ADD JAR hiveudf-1.0-SNAPSHOT.jar;
CREATE TEMPORARY FUNCTION getnetworks AS 'com.griddynamics.GetNetworks';

CREATE TABLE event_networks AS
  SELECT ev.name, ev.price, ev.created, ev.category, ev.ip, nw.network
  FROM events ev
  LATERAL VIEW getnetworks(ev.ip) nw AS network;

SELECT x.country_name, sum(x.price) as spendings
FROM (
  SELECT DISTINCT eventnetworks.name, eventnetworks.price, eventnetworks.created, geocountry.country_name
  FROM event_networks eventnetworks
    INNER JOIN geo_ip geoip on geoip.network = eventnetworks.network
    INNER JOIN geo_country geocountry on geocountry.geoname_id = geoip.geoname_id
  WHERE geocountry.geoname_id IS NOT NULL
) x
GROUP BY x.country_name
ORDER BY spendings DESC
LIMIT 5;


/*SELECT DISTINCT eventnetworks.name, eventnetworks.price, eventnetworks.created, geocountry.country_name
FROM event_networks eventnetworks
  INNER JOIN geo_ip geoip on geoip.network = eventnetworks.network
  INNER JOIN geo_country geocountry on geocountry.geoname_id = geoip.geoname_id
WHERE geocountry.geoname_id IS NOT NULL;*/




/*
TODO!!!
select DISTINCT t.name, geoip.geoname_id, t.price, t.network
FROM (
  SELECT ev.name, ev.price, nw.network
  FROM events ev
  LATERAL VIEW getnetworks(ev.ip) nw AS network
) t
  INNER JOIN geo_ip geoip on geoip.network = t.network
LIMIT 100;*/

