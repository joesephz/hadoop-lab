package com.griddynamics;

import org.apache.commons.net.util.SubnetUtils;
import org.apache.hadoop.hive.ql.exec.Description;
import org.apache.hadoop.hive.ql.exec.UDF;

@Description(
    name = "checkipinsubnetwork",
    value = "_FUNC_(string, string) - checks ip belongs to subnet"
)
public class CheckIpInSubnetwork extends UDF {
  public boolean evaluate(String ip, String subnetMask) {
    return new SubnetUtils(subnetMask).getInfo().isInRange(ip);
  }
}
