package com.griddynamics;

import static org.junit.Assert.assertNotNull;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.hive.HiveContext;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

public class Top10CountriesDriverTest extends AbstractTest {
  private static DataFrame events;
  private static DataFrame geoIps;
  private static DataFrame geoCountries;
  private static Top10CountriesDriver driver;

  @BeforeClass
  public static void beforeClass() {
    AbstractTest.beforeClass();
    sqlContext = new HiveContext(sc);
    driver = new Top10CountriesDriver(sc, sqlContext);
    events = loadEvents(sqlContext);
    geoIps = loadGeoIps(sqlContext);
    geoCountries = loadGeoCountries(sqlContext);
  }

  @Test
  public void process() {
    JavaRDD<Top10CountriesDriver.Top10Countries> resultRDD = driver.process(events.toJavaRDD(), geoCountries.toJavaRDD(), geoIps.toJavaRDD());
    assertNotNull(resultRDD);
    List<Top10CountriesDriver.Top10Countries> result = resultRDD.collect();
    result.forEach(System.out::println);
  }

  @Test
  public void processWithDF() {
    DataFrame result = driver.process(events, geoCountries, geoIps);
    assertNotNull(result);
    result.show();
  }
}
