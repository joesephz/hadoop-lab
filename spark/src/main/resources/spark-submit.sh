#!/usr/bin/env bash

spark-submit \
--master yarn \
--deploy-mode=client \
--driver-memory 256M \
--executor-memory 512M \
--conf spark.test.db.url=jdbc:mysql://10.0.0.21:3306/izeldin \
--conf spark.test.db.driver=com.mysql.jdbc.Driver \
--conf spark.test.db.user=root \
--conf spark.test.db.password= \
--class com.griddynamics.Top10CategoriesDriver \
spark-1.0-SNAPSHOT.jar \
/flume/events
