alter table events add partition (dt = '2005-08-03') location '/flume/events/2005/08/03';
alter table events add partition (dt = '2006-08-21') location '/flume/events/2006/08/21';
alter table events add partition (dt = '2007-06-04') location '/flume/events/2007/06/04';
alter table events add partition (dt = '2007-08-25') location '/flume/events/2007/08/25';
alter table events add partition (dt = '2007-11-17') location '/flume/events/2007/11/17';
