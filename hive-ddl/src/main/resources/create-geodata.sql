CREATE TABLE geo_ip (
  network STRING,
  geoname_id int,
  registered_country_geoname_id int,
  represented_country_geoname_id int,
  is_anonymous_proxy int,
  is_satellite_provider int
)
ROW FORMAT DELIMITED FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' STORED AS TEXTFILE;

CREATE TABLE geo_country (
  geoname_id int,
  locale_code string,
  continent_code string,
  continent_name string,
  country_iso_code string,
  country_name string,
  is_in_european_union int
)
ROW FORMAT DELIMITED FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' STORED AS TEXTFILE;

LOAD DATA INPATH './GeoLite2-Country-Blocks-IPv4.csv' OVERWRITE INTO TABLE geo_ip;
LOAD DATA INPATH './GeoLite2-Country-Locations-en.csv' OVERWRITE INTO TABLE geo_country;


