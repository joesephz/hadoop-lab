CREATE TABLE TOP10CATEGORIES (
  category VARCHAR(100),
  count integer
);

CREATE TABLE TOP10PRODUCTS (
  category VARCHAR(100),
  name VARCHAR(100),
  count integer
);

CREATE TABLE TOP10COUNTRIES (
  country VARCHAR(100),
  amount double
);

