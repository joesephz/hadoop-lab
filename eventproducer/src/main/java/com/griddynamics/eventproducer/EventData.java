package com.griddynamics.eventproducer;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class EventData {
  String name;
  double price;
  LocalDateTime dateTime;
  String category;
  String ip;

  @Override
  public String toString() {
    return String.join(",",
        name,
        String.format("%.2f", price),
        dateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")),
        category,
        ip
    );
  }
}
