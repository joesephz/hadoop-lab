package com.griddynamics.eventproducer;

public class Main {

  public static void main(String[] args) throws Exception {
    FlumeGenerator flumeGenerator = new FlumeGenerator();
    SocketSender sender = new SocketSender(args[0], Integer.valueOf(args[1]));

    for (int i = 0; i < 10000; i++) {
      sender.send(flumeGenerator.generate());
      // System.out.println(flumeGenerator.generate());
    }
    sender.close();
  }

}
