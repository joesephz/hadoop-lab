package com.griddynamics.eventproducer;

import java.time.LocalDateTime;
import java.util.Random;

public class FlumeGenerator {
  private static final int SECONDS_IN_WEEK = 7 * 24 * 60 * 60;

  private Random nameRandom = new Random();
  private Random priceRandom = new Random();
  private Random timeRandom = new Random();
  private Random ipRandom = new Random();

  public static int getNextGaussian(Random random, int deviation) {
    int result = deviation + 1;
    while (result > deviation) {
      result = (int) (Math.abs(random.nextGaussian()) * deviation);
    }
    return result;
  }

  public EventData generate() {
    EventData eventData = new EventData();

    eventData.category = "" + (char) (97 + nameRandom.nextInt(10));
    eventData.name = eventData.category + nameRandom.nextInt(10);

    eventData.price = Math.abs(priceRandom.nextGaussian()) * 100;

    eventData.dateTime = LocalDateTime.now()
        .minusSeconds(getNextGaussian(timeRandom, SECONDS_IN_WEEK));

    eventData.ip = ipRandom.nextInt(256) + "."
        + ipRandom.nextInt(256) + "."
        + ipRandom.nextInt(256) + "."
        + ipRandom.nextInt(256);

    return eventData;
  }

}
