#!/usr/bin/env bash

basedir="/flume/events/"
basedir_e="\/flume\/events\/"

hadoop fs -ls -R "$basedir" | \
sed -n "s/^.*\([0-9]\{4\}\)\/\([0-9]\{2\}\)\/\([0-9]\{2\}\).*$/alter table izeldin.events add partition (dt = '\1-\2-\3') location '$basedir_e\1\/\2\/\3';/p" | \
sort | \
uniq > add_partitions.sql
hive -f add_partitions.sql
