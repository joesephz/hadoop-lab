package com.griddynamics;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.junit.BeforeClass;
import org.junit.Test;

public class AbstractTest {
  protected static JavaSparkContext sc;
  protected static SQLContext sqlContext;

  @BeforeClass
  public static void beforeClass() {
    SparkConf sparkConf = new SparkConf();
    sparkConf.setAppName("Unit Test");
    sparkConf.setMaster("local[2]");

    sc = new JavaSparkContext(sparkConf);
    sqlContext = new SQLContext(sc);
  }

  protected static DataFrame loadEvents(SQLContext sqlContext) {
    String csv = AbstractTest.class.getResource("/events.csv").getPath();
    return sqlContext.read()
        .format("com.databricks.spark.csv")
        .option("header", "false")
        .option("comment", null)
        .option("delimiter", ",")
        .option("quote", null)
        .schema(new StructType(new StructField[]{
            new StructField("name", DataTypes.StringType, false, Metadata.empty()),
            new StructField("price", DataTypes.DoubleType, false, Metadata.empty()),
            new StructField("dateTime", DataTypes.TimestampType, false, Metadata.empty()),
            new StructField("category", DataTypes.StringType, false, Metadata.empty()),
            new StructField("ip", DataTypes.StringType, false, Metadata.empty())
        }))
        .load(csv);
  }

  protected static DataFrame loadGeoIps(SQLContext sqlContext) {
    String csv = AbstractTest.class.getResource("/GeoLite2-Country-Blocks-IPv4.csv").getPath();
    return sqlContext.read()
        .format("com.databricks.spark.csv")
        .option("header", "false")
        .option("comment", null)
        .option("delimiter", ",")
        .option("quote", null)
        .schema(new StructType(new StructField[]{
            new StructField("network", DataTypes.StringType, false, Metadata.empty()),
            new StructField("geoname_id", DataTypes.IntegerType, true, Metadata.empty()),
            new StructField("registered_country_geoname_id", DataTypes.IntegerType, true, Metadata.empty()),
            new StructField("represented_country_geoname_id", DataTypes.IntegerType, true, Metadata.empty()),
            new StructField("is_anonymous_proxy", DataTypes.IntegerType, true, Metadata.empty()),
            new StructField("is_satellite_provider", DataTypes.IntegerType, true, Metadata.empty())
        }))
        .load(csv);
  }

  protected static DataFrame loadGeoCountries(SQLContext sqlContext) {
    String csv = AbstractTest.class.getResource("/GeoLite2-Country-Locations-en.csv").getPath();
    return sqlContext.read()
        .format("com.databricks.spark.csv")
        .option("header", "false")
        .option("comment", null)
        .option("delimiter", ",")
        .option("quote", "\"")
        .schema(new StructType(new StructField[]{
            new StructField("geoname_id", DataTypes.IntegerType, false, Metadata.empty()),
            new StructField("locale_code", DataTypes.StringType, true, Metadata.empty()),
            new StructField("continent_code", DataTypes.StringType, true, Metadata.empty()),
            new StructField("continent_name", DataTypes.StringType, true, Metadata.empty()),
            new StructField("country_iso_code", DataTypes.StringType, true, Metadata.empty()),
            new StructField("country_name", DataTypes.StringType, true, Metadata.empty()),
            new StructField("is_in_european_union", DataTypes.IntegerType, true, Metadata.empty())
        }))
        .load(csv);
  }

  @Test
  public void testEvents() {
    DataFrame df = loadEvents(sqlContext);
    assertNotNull(df);
    assertEquals(20, df.count());

    DataFrame geoIps = loadGeoIps(sqlContext);
    assertNotNull(geoIps);
    assertTrue(geoIps.count() > 0);

    DataFrame geoCountries = loadGeoCountries(sqlContext);
    assertNotNull(geoCountries);
    assertTrue(geoCountries.count() > 0);
  }

}
