package com.griddynamics;

import static org.junit.Assert.assertNotNull;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.DataFrame;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

public class Top10CategoriesDriverTest extends AbstractTest {
  private static DataFrame events;
  private static Top10CategoriesDriver driver;

  @BeforeClass
  public static void beforeClass() {
    AbstractTest.beforeClass();
    driver = new Top10CategoriesDriver(sc, sqlContext);
    events = loadEvents(sqlContext);
  }

  @Test
  public void process() {
    JavaRDD<Top10CategoriesDriver.Top10Categories> resultRDD = driver.process(events.toJavaRDD());
    assertNotNull(resultRDD);
    List<Top10CategoriesDriver.Top10Categories> result = resultRDD.collect();
    result.forEach(System.out::println);
  }

  @Test
  public void processWithDF() {
    DataFrame result = driver.process(events);
    assertNotNull(result);
    result.show();
  }
}
