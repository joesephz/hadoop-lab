package com.griddynamics;

import static java.util.stream.Collectors.toList;
import static org.apache.spark.sql.functions.asc;
import static org.apache.spark.sql.functions.desc;
import static org.apache.spark.sql.functions.row_number;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.expressions.Window;
import org.apache.spark.sql.expressions.WindowSpec;
import org.apache.spark.sql.hive.HiveContext;

import scala.Tuple2;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Top10ProductsDriver extends AbstractDriver {
  private static int LIMIT = 5;

  Top10ProductsDriver(JavaSparkContext jsc, SQLContext sqlContext) {
    super(jsc, sqlContext);
  }

  public static void main(String[] args) {
    if (args.length < 1) {
      System.err.println("Usage: Top10Categories <path to files>");
      System.exit(1);
    }

    try (JavaSparkContext jsc = new JavaSparkContext()) {
      Top10ProductsDriver top10Categories = new Top10ProductsDriver(jsc, new HiveContext(jsc));
      top10Categories.run(args[0]);
    }
  }

  private static List<Tuple2<String, Integer>> addProduct(List<Tuple2<String, Integer>> list, Tuple2<String, Integer> item) {
    list.add(item);
    if (list.size() > LIMIT) {
      list.sort((o1, o2) -> o2._2.equals(o1._2) ? o2._2.compareTo(o1._2) : o2._1.compareTo(o1._1));
      list.remove(LIMIT - 1);
    }
    return list;
  }

  private static List<Tuple2<String, Integer>> addProducts(List<Tuple2<String, Integer>> left, List<Tuple2<String, Integer>> right) {
    List<Tuple2<String, Integer>> result = new ArrayList<>(left.size() + right.size());
    result.addAll(left);
    result.addAll(right);
    result.sort((o1, o2) -> o2._2.equals(o1._2) ? o2._2.compareTo(o1._2) : o2._1.compareTo(o1._1));

    if (result.size() > LIMIT) {
      result = result.subList(0, LIMIT);
    }
    return result;
  }

  private void run(String dir) {
   /* JavaRDD<Row> rows = readEvents(dir).javaRDD();

    JavaRDD<Top10Products> result = process(rows);

    write(dbContext.createDataFrame(result, Top10Products.class), "TOP10PRODUCTS");*/

    DataFrame src = readEvents(dir);

    DataFrame processed = process(src);

    write(processed, "TOP10PRODUCTS");
  }

  JavaRDD<Top10Products> process(JavaRDD<Row> rows) {
    return rows
        .mapToPair(row -> new Tuple2<>(new Tuple2<>(row.getString(0), row.getString(3)), 1))
        .reduceByKey((i1, i2) -> i1 + i2)
        .mapToPair(t -> new Tuple2<>(t._1._2, new Tuple2<>(t._1._1, t._2)))
        .aggregateByKey(
            new ArrayList<>(),
            Top10ProductsDriver::addProduct,
            Top10ProductsDriver::addProducts
        )
        .flatMap(t -> t._2.stream().map(e -> new Top10Products(t._1, e._1, e._2)).collect(toList()));
  }

  DataFrame process(DataFrame src) {
    WindowSpec w = Window.partitionBy("category").orderBy(desc("count"));

    return src
        .groupBy("name", "category")
        .count()
        .sort(asc("category"), desc("count"), asc("name"))
        .withColumn("rn", row_number().over(w))
        .where("rn <= 5")
        .drop("rn");
  }

  public static class Top10Products implements Serializable {
    private String category;
    private String name;
    private int count;

    public Top10Products(String category, String name, int count) {
      this.category = category;
      this.name = name;
      this.count = count;
    }

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public String getCategory() {
      return category;
    }

    public void setCategory(String category) {
      this.category = category;
    }

    public int getCount() {
      return count;
    }

    public void setCount(int count) {
      this.count = count;
    }
  }
}
