package com.griddynamics;

import static org.apache.spark.sql.functions.desc;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;

import scala.Tuple2;

import java.io.Serializable;

public class Top10CategoriesDriver extends AbstractDriver {
  private static int LIMIT = 5;

  Top10CategoriesDriver(JavaSparkContext jsc, SQLContext sqlContext) {
    super(jsc, sqlContext);
  }

  public static void main(String[] args) {
    if (args.length < 1) {
      System.err.println("Usage: Top10Categories <path to files>");
      System.exit(1);
    }

    try (JavaSparkContext jsc = new JavaSparkContext()) {
      Top10CategoriesDriver top10CategoriesDriver = new Top10CategoriesDriver(jsc, new SQLContext(jsc));
      top10CategoriesDriver.run(args[0]);
    }
  }

  private void run(String dir) {
/*
    JavaRDD<Row> rows = readEvents(dir).javaRDD();

    JavaRDD<Top10Categories> result = process(rows);

    write(dbContext.createDataFrame(result, Top10Categories.class).limit(10), "TOP10CATEGORIES");
*/
    DataFrame src = readEvents(dir);

    DataFrame processed = process(src);

    write(processed, "TOP10CATEGORIES");
  }

  JavaRDD<Top10Categories> process(JavaRDD<Row> rows) {
    return rows
        .mapToPair(row -> new Tuple2<>(row.getString(3), 1))
        .reduceByKey((i1, i2) -> i1 + i2)
        .map(t -> new Top10Categories(t._2, t._1))
        .sortBy(Top10Categories::getCount, false, 1);
  }

  DataFrame process (DataFrame src) {
    return src
        .groupBy("category")
        .count()
        .sort(desc("count"))
        .limit(LIMIT)
        .toDF("category", "count");
  }

  public static class Top10Categories implements Serializable {
    private int count;
    private String category;

    public Top10Categories(int count, String category) {
      this.count = count;
      this.category = category;
    }

    public int getCount() {
      return count;
    }

    public void setCount(int count) {
      this.count = count;
    }

    public String getCategory() {
      return category;
    }

    public void setCategory(String category) {
      this.category = category;
    }

    @Override
    public String toString() {
      final StringBuilder sb = new StringBuilder("Top10Categories{");
      sb.append("count=").append(count);
      sb.append(", category='").append(category).append('\'');
      sb.append('}');
      return sb.toString();
    }
  }

}
