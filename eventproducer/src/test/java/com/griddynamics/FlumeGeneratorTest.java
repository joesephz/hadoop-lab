package com.griddynamics;

import com.griddynamics.eventproducer.EventData;
import com.griddynamics.eventproducer.FlumeGenerator;

import org.junit.Test;

public class FlumeGeneratorTest {
  private static final int SECONDS_IN_WEEK = 7 * 24 * 60 * 60;

  @Test
  public void generate() {
    FlumeGenerator flumeGenerator = new FlumeGenerator();
    EventData result = flumeGenerator.generate();
    System.out.println(result);
  }
}
