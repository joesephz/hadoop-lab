package com.griddynamics;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.Metadata;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import java.io.IOException;
import java.util.Properties;

public class AbstractDriver {
  protected JavaSparkContext sc;
  protected SQLContext sqlContext;

  protected AbstractDriver(JavaSparkContext jsc, SQLContext sqlContext) {
    this.sc = jsc;
    this.sqlContext = sqlContext;
  }

  protected DataFrame readEvents(String dir) {
    Path src = new Path(dir);

    try {
      Configuration confFS = new Configuration();
      confFS.addResource("/etc/hadoop/conf/core-site.xml");
      confFS.addResource("/etc/hadoop/conf/hdfs-site.xml");
      FileSystem fs =  FileSystem.newInstance(confFS);
      
      if (fs.exists(src) && fs.isDirectory(src)) {
        return sqlContext.read()
            .format("com.databricks.spark.csv")
            .option("header", "false")
            .option("comment", null)
            .option("delimiter", ",")
            .option("quote", null)
            .schema(new StructType(new StructField[] {
                new StructField("name", DataTypes.StringType, false, Metadata.empty()),
                new StructField("price", DataTypes.DoubleType, false, Metadata.empty()),
                new StructField("dateTime", DataTypes.TimestampType, false, Metadata.empty()),
                new StructField("category", DataTypes.StringType, false, Metadata.empty()),
                new StructField("ip", DataTypes.StringType, false, Metadata.empty())
            }))
            .load(src.toString()+"/**/**/**/*.csv");

      } else {
        throw new IOException("" + dir + " does not exists in HDFS");
      }
    } catch (IOException e) {
      throw new RuntimeException("Error reading files", e);
    }
  }

  protected DataFrame readFromHive(String tableName) {
    return sqlContext.table("izeldin." + tableName);
  }

  protected void write (DataFrame df, String tableName) {
    Properties connectionProperties = new Properties();
    SparkConf sparkConf = sc.getConf();
    connectionProperties.setProperty("driver", sparkConf.get("spark.test.db.driver"));
    connectionProperties.setProperty("user", sparkConf.get("spark.test.db.user"));
    connectionProperties.setProperty("password", sparkConf.get("spark.test.db.password"));

    //System.out.println("Connecting to MySQL url:"+ sparkConf.get("spark.test.db.url")+" user:'"+connectionProperties.getProperty("user")+"' password:'"+connectionProperties.getProperty("password")+"'");
    df.write().mode(SaveMode.Overwrite).jdbc(sparkConf.get("spark.test.db.url"), tableName, connectionProperties);
  }


}
