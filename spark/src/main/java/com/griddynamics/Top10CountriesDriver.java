package com.griddynamics;

import static java.util.stream.Collectors.toList;
import static org.apache.spark.sql.functions.callUDF;
import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.explode;
import static org.apache.spark.sql.functions.sum;

import org.apache.commons.net.util.SubnetUtils;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.api.java.UDF1;
import org.apache.spark.sql.hive.HiveContext;
import org.apache.spark.sql.types.DataTypes;

import scala.Tuple2;

import java.io.Serializable;
import java.util.List;
import java.util.stream.IntStream;

public class Top10CountriesDriver extends AbstractDriver {
  private static int LIMIT = 5;

  Top10CountriesDriver(JavaSparkContext jsc, SQLContext sqlContext) {
    super(jsc, sqlContext);
  }

  public static void main(String[] args) {
    if (args.length < 1) {
      System.err.println("Usage: Top10Countries <path to files>");
      System.exit(1);
    }

    try (JavaSparkContext jsc = new JavaSparkContext()) {
      Top10CountriesDriver driver = new Top10CountriesDriver(jsc, new HiveContext(jsc));
      driver.run(args[0]);
    }
  }

  private static List<String> getIpRanges(String ip) {
    return IntStream.rangeClosed(10, 32)
        .mapToObj(i -> new SubnetUtils(ip + "/" + i).getInfo().getNetworkAddress() + "/" + i)
        .collect(toList());
  }

  /*
    private void run(String dir) {
      JavaRDD<Row> events = readEvents(dir).javaRDD();
      JavaRDD<Row> countries = readFromHive("geo_country").javaRDD();
      JavaRDD<Row> ips = readFromHive("geo_ip").javaRDD();

      JavaRDD<Top10Countries> result = process(events, countries, ips);

      write(dbContext.createDataFrame(result, Top10Countries.class).limit(LIMIT), "TOP10COUNTRIES");
    }

  */
  private void run(String dir) {
    DataFrame events = readEvents(dir);
    DataFrame countries = readFromHive("geo_country");
    DataFrame ips = readFromHive("geo_ip");

    DataFrame result = process(events, countries, ips);

    write(result, "TOP10COUNTRIES");
  }

  JavaRDD<Top10Countries> process(JavaRDD<Row> events, JavaRDD<Row> countries, JavaRDD<Row> ips) {

    JavaPairRDD<String, Double> priceForIpRange = events
        .mapToPair(row -> new Tuple2<>(row.getDouble(1), row.getString(4))) // price->ip
        .mapValues(Top10CountriesDriver::getIpRanges)
        .flatMapToPair(t -> t._2.stream().map(e -> new Tuple2<>(e, t._1)).collect(toList()));

    JavaPairRDD<Integer, String> countriesRDD =
        countries.mapToPair(row -> new Tuple2<>(row.getAs("geoname_id"), row.getAs("country_name")));

    JavaPairRDD<Integer, String> ipRDD =
        ips.mapToPair(row -> new Tuple2<>(row.getAs("geoname_id"), row.getAs("network")));

    JavaPairRDD<String, String> ipRangeToCountryRDD = ipRDD
        .join(countriesRDD)
        .values()
        .mapToPair(t -> t);

    return priceForIpRange
        .join(ipRangeToCountryRDD)
        .values()
        .mapToPair(Tuple2::swap)
        .aggregateByKey(
            0.0,
            (a, b) -> a + b,
            (a, b) -> a + b
        )
        .map(t -> new Top10Countries(t._1, t._2))
        .sortBy(Top10Countries::getAmount, false, 1);
  }

  DataFrame process(DataFrame events, DataFrame countries, DataFrame ips) {
    UDF1 getIpRangesUDF = (UDF1<String, String[]>) ip -> getIpRanges(ip).toArray(new String[0]);

    sqlContext.udf().register("getIpRanges", getIpRangesUDF, DataTypes.createArrayType(DataTypes.StringType));

    return events
        .select(col("price"), explode(callUDF("getIpRanges", col("ip"))).as("network"))
        .join(ips.join(countries, "geoname_id"), "network")
        .groupBy(col("country_name").as("country"))
        .agg(sum("price").as("amount"))
        .select("country", "amount")
        .sort(col("amount").desc())
        .limit(LIMIT);
  }

  public static class Top10Countries implements Serializable {
    private String country;
    private double amount;

    public Top10Countries(String country, double amount) {
      this.country = country;
      this.amount = amount;
    }

    public String getCountry() {
      return country;
    }

    public void setCountry(String country) {
      this.country = country;
    }

    public double getAmount() {
      return amount;
    }

    public void setAmount(double amount) {
      this.amount = amount;
    }
  }
}
