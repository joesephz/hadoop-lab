SELECT y.category,
       y.name,
       y.cnt
FROM (
  SELECT x.name,
         x.category,
         row_number() over(partition by x.category order by x.cnt desc) as rn,
         x.cnt
  FROM (
      SELECT name,
         category,
         count(created) as cnt
      FROM events
      GROUP BY category, name
      ORDER BY category, cnt DESC, name
  ) x
) y
WHERE y.rn<=5;
