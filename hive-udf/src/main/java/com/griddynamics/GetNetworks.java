package com.griddynamics;

import org.apache.commons.net.util.SubnetUtils;
import org.apache.hadoop.hive.ql.exec.Description;
import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDTF;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.ObjectInspectorFactory;
import org.apache.hadoop.hive.serde2.objectinspector.PrimitiveObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.StructObjectInspector;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;

import java.util.Collections;

@Description(
    name = "getnetworks",
    value = "_FUNC_(string) - get all possible networks"
)
public class GetNetworks extends GenericUDTF {
  private transient PrimitiveObjectInspector stringOI = null;

  public GetNetworks() {
  }

  @Override
  public StructObjectInspector initialize(ObjectInspector[] args) throws UDFArgumentException {
    if (args.length != 1) {
      throw new UDFArgumentException("NameParserGenericUDTF() takes exactly one argument");
    }

    if (args[0].getCategory() != ObjectInspector.Category.PRIMITIVE && ((PrimitiveObjectInspector) args[0]).getPrimitiveCategory() != PrimitiveObjectInspector.PrimitiveCategory.STRING) {
      throw new UDFArgumentException("NameParserGenericUDTF() takes a string as a parameter");
    }

    // input inspectors
    stringOI = (PrimitiveObjectInspector) args[0];

    return ObjectInspectorFactory.getStandardStructObjectInspector(
        Collections.singletonList("network"),
        Collections.<ObjectInspector>singletonList(PrimitiveObjectInspectorFactory.javaStringObjectInspector)
    );
  }

  @Override
  public void process(Object[] objects) throws HiveException {
    final String ip = stringOI.getPrimitiveJavaObject(objects[0]).toString();

    for (int i = 10; i <= 32; i++) {
      forward(new Object[]{new SubnetUtils(ip + "/" + i).getInfo().getNetworkAddress() + "/" + i});
    }
  }


  @Override
  public void close() throws HiveException {

  }

  @Override
  public String toString() {
    return "getnetworks";
  }
}
