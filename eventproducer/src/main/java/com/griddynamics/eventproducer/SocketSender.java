package com.griddynamics.eventproducer;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

public class SocketSender {
  private String host;
  private int port;
  private PrintWriter writer;

  public SocketSender(String host, int port) throws IOException {
    this.host = host;
    this.port = port;
    getWriter();
  }

  private PrintWriter getWriter() throws IOException {
    if (writer == null) {
      Socket clientSocket = new Socket(host, port);
      DataOutputStream os = new DataOutputStream(clientSocket.getOutputStream());
      writer = new PrintWriter(os, true);
    }
    return writer;
  }

  public void send(EventData eventData) throws IOException {
    System.out.println(eventData);
    getWriter().println(eventData.toString());
  }

  public void close() throws IOException {
    if (writer != null) {
      writer.close();
      writer = null;
    }
  }
}
